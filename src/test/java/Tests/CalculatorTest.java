package Tests;


import Base.TestBase;
import Screens.CalculatorMainScreen;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CalculatorTest extends TestBase
{

    @Test
    public void validarDivisaoInformandoDoisNumerosDoTipoInteiroComSucesso(){
        CalculatorMainScreen calculatorMainScreen = new CalculatorMainScreen();
        calculatorMainScreen.ClicarBotaoEight();
        calculatorMainScreen.ClicarBotaoZero();
        calculatorMainScreen.ClicarBotaoDivision();
        calculatorMainScreen.ClicarBotaoTwo();
        Assert.assertEquals(40,Integer.parseInt(calculatorMainScreen.resultTextView()));
    }

    @Test
    public void validarSomaInformandoDoisNumerosDoTipoInteiroComSucesso()
    {
        CalculatorMainScreen calculatorMainScreen = new CalculatorMainScreen();
        calculatorMainScreen.ClicarBotaoFive();
        calculatorMainScreen.ClicarBotaoFour();
        calculatorMainScreen.ClicarBotaoSum();
        calculatorMainScreen.ClicarBotaoFour();
        calculatorMainScreen.ClicarBotaoSix();
        Assert.assertEquals(100,Integer.parseInt(calculatorMainScreen.resultTextView()));
    }

    @Test
    public void validarMultiplicacaoInformandoDoisNumerosDoTipoInteiroComSucesso()
    {
        CalculatorMainScreen calculatorMainScreen = new CalculatorMainScreen();
        calculatorMainScreen.ClicarBotaoFive();
        calculatorMainScreen.ClicarBotaoZero();
        calculatorMainScreen.ClicarBotaoZero();
        calculatorMainScreen.ClicarBotaoMultiplication();
        calculatorMainScreen.ClicarBotaoZero();
        Assert.assertEquals(0,Integer.parseInt(calculatorMainScreen.resultTextView()));
    }

    @Test
    public void validarSubtracaoInformandoDoisNumerosInteirosComSucesso()
    {
        CalculatorMainScreen calculatorMainScreen = new CalculatorMainScreen();
        calculatorMainScreen.ClicarBotaoNine();
        calculatorMainScreen.ClicarBotaoSubtraction();
        calculatorMainScreen.ClicarBotaoSeven();
        Assert.assertEquals("2",String.valueOf(calculatorMainScreen.resultTextView()));
    }

}
