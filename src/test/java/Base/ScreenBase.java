package Base;

import factory.DriverFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//PageBase
public class ScreenBase {

    private AppiumDriver driver = null;
    private WebDriverWait wait = null;

    public ScreenBase()  {
        driver = DriverFactory.INSTANCE;
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
        wait = new WebDriverWait(driver,10);
    }

    protected void waitForElement(MobileElement element)
    {
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected void waitForBy(By locator)
    {
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    protected void click(MobileElement element)
    {
        waitForElement(element);
        element.click();
    }

    protected void sendKeys(MobileElement element, String text)
    {
        waitForElement(element);
        element.sendKeys(text);
    }

    protected String getText(MobileElement element)
    {
        waitForElement(element);
        String text = element.getText();
        return text;
    }
}
