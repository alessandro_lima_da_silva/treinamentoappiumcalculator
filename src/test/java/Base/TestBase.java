package Base;

import factory.DriverFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import java.io.IOException;

//TestBase
public class TestBase {

    @BeforeMethod (alwaysRun = true)
    public void beforeTest() throws IOException
    {
        DriverFactory.CreateInstance();

    }

    @AfterMethod (alwaysRun = true)
    public void afterTest()
    {
        DriverFactory.QuitInstance();
    }
}
