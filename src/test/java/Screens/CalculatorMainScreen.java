package Screens;

import Base.ScreenBase;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class CalculatorMainScreen extends ScreenBase {

    @AndroidFindBy(xpath = "//android.widget.Button[@text='0']")
    private MobileElement zeroButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='1']")
    private MobileElement oneButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='2']")
    private MobileElement twoButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='3']")
    private MobileElement treeButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='4']")
    private MobileElement fourButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='5']")
    private MobileElement fiveButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='6']")
    private MobileElement sixButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='7']")
    private MobileElement sevenButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='8']")
    private MobileElement eightButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='9']")
    private MobileElement nineButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='+']")
    private MobileElement sumButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='−']")
    private MobileElement subtractionButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='×']")
    private MobileElement multiplicationButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='÷']")
    private MobileElement divisionButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='.']")
    private MobileElement pointButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='=']")
    private MobileElement equalButton;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='DEL']")
    private MobileElement delButton;
    @AndroidFindBy(xpath = "//android.widget.TextView[@index = '2' ]")
    private MobileElement resultTextView;
    @AndroidFindBy(xpath = "//android.widget.TextView[@index = '1' ]")
    private MobileElement formulaTextView;
    @AndroidFindBy(xpath = "//android.widget.TextView[@index = '0' ]")
    private MobileElement modoDeCalculoTextView;

    public void ClicarBotaoZero()
    {
        click(zeroButton);
    }

    public void ClicarBotaoOne()
    {
        click(oneButton);
    }

    public void ClicarBotaoTwo()
    {
        click(twoButton);
    }

    public void ClicarBotaoTree()
    {
        click(treeButton);
    }

    public void ClicarBotaoFour()
    {
        click(fourButton);
    }

    public void ClicarBotaoFive()
    {
        click(fiveButton);
    }

    public void ClicarBotaoSix()
    {
        click(sixButton);
    }

    public void ClicarBotaoSeven()
    {
        click(sevenButton);
    }

    public void ClicarBotaoEight()
    {
        click(eightButton);
    }
    public void ClicarBotaoNine()
    {
        click(nineButton);
    }

    public void ClicarBotaoSum()
    {
        click(sumButton);
    }

    public void ClicarBotaoSubtraction()
    {
        click(subtractionButton);
    }

    public void ClicarBotaoMultiplication()
    {
        click(multiplicationButton);
    }

    public void ClicarBotaoDivision()
    {
        click(divisionButton);
    }

    public void ClicarBotaoPoint()
    {
        click(pointButton);
    }

    public void ClicarBotaoEqual()
    {
        click(equalButton);
    }

    public void ClicarBotaoDel()
    {
        click(delButton);
    }

    public String resultTextView()
    {
        return getText(resultTextView);
    }

    public String formulaTextView()
    {
        return getText(formulaTextView);
    }

    public String getModoDeCalculoTextView()
    {
        return getText(modoDeCalculoTextView);
    }

}
