package factory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class DriverFactory {

    public static AppiumDriver<MobileElement> INSTANCE;

    public static void CreateInstance() throws MalformedURLException {

        if(INSTANCE == null)
        {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("platformName","Android");
            caps.setCapability("platformVersion","7.0");
            caps.setCapability("deviceName","Pixel_2_API_Nougat_7");
            caps.setCapability("appPackage","com.android.calculator2");
            caps.setCapability("appActivity","com.android.calculator2.Calculator");
            caps.setCapability("udid","emulator-5554");
            caps.setCapability("autoGrantPermissions", "true");
            INSTANCE = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), caps);

        }

    }

    public static void QuitInstance()
    {
        INSTANCE.quit();
        INSTANCE = null;
    }

}
